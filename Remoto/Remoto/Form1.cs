﻿using System;
using System.Windows.Forms;
using System.Net.Sockets; //Libreria usada para realizar conexiones TCP/IP
using System.Drawing; //Libreria que permite dibujar
using System.Drawing.Imaging;
using System.Runtime.Serialization.Formatters.Binary; //Libreria para conversion a datos binarios

namespace Remoto
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Declaración de variables
        /// </summary>
        private readonly TcpClient cliente = new TcpClient();
        private NetworkStream transmision;
        private int puerto;

        private static Image tomarEscritorio()
        {
            //Se dibuja el rectangulo principal de la pantalla
            Rectangle bounds = Screen.PrimaryScreen.Bounds;
            Bitmap screenshot = new Bitmap(bounds.Width, bounds.Height, PixelFormat.Format32bppArgb); //Se crea screenshot en mapa de bits
            Graphics grafico = Graphics.FromImage(screenshot);
            grafico.CopyFromScreen(bounds.X, bounds.Y, 0, 0, bounds.Size, CopyPixelOperation.SourceCopy);
            return screenshot;
        }
        //Método que se encarga del envio del escritorio
        private void enviarEscritorio()
        {
            //Se convierte al imagen del escritorio a binario
            //Ya que solo pueden enviarse este tipo de datos por la red
            BinaryFormatter formatear = new BinaryFormatter();
            transmision = cliente.GetStream();
            formatear.Serialize(transmision, tomarEscritorio());
        }

        public Form1()
        {
            InitializeComponent();
        }
        //Salir de la aplicacion
        private void lblSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //Método que funciona al dar click en el boton de conectar.
        private void btnConectar_Click(object sender, EventArgs e)
        {
            //Se obtiene el valor del puerto
            puerto = int.Parse(txtPuerto.Text);
            //Control de excepciones
            try
            {
                //Se crea la conexión
                cliente.Connect(txtIP.Text, puerto);
                MessageBox.Show("Conectado");
            }catch(Exception ex)
            {
                //Se muestra el error ocurrido
                MessageBox.Show("Ocurrio un problema \n" + ex.Message);
            }
        }
        //Métdo que se ejecuta al da click en compartir
        private void btnCompartir_Click(object sender, EventArgs e)
        {
            if (btnCompartir.Text == "Compartir")
            {
                tmrCompartir.Start();
                btnCompartir.Text = "Detener";
            }
            else
            {
                tmrCompartir.Stop();
                btnCompartir.Text = "Compartir";
            }
        }
        //Método que funciona con el timer
        private void tmrCompartir_Tick(object sender, EventArgs e)
        {
            //Se comparte el escritorio
            enviarEscritorio();
        }
    }
}
