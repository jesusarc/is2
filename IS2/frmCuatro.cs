﻿using System;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmCuatro : Form
    {
        //Declaración de variables para incremenatr campos en el grafico
        public static int vX, vY, X, Y, A, B;
        public frmCuatro()
        {
            InitializeComponent();
        }
        //Botón para salir de la aplicación.
        private void lblSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 menu = new Form1();
            menu.Show();
        }

        //Método que funciona durante la carga del sistema
        private void frmCuatro_Load(object sender, EventArgs e)
        {
            //Se convierten las graficas en 3D
            chGrafica.ChartAreas[0].Area3DStyle.Enable3D = true;
            chGrafica2.ChartAreas[0].Area3DStyle.Enable3D = true;
            chMousem.ChartAreas[0].Area3DStyle.Enable3D = true;
            //Se cargan los datos de la gráfica dos
            chGrafica2.Series[0].Points.AddXY(X++, datos.practica1);
            chGrafica2.Series[1].Points.AddXY(X++, datos.practica2);
            chGrafica2.Series[2].Points.AddXY(X++, datos.practica3);
            chGrafica2.Series[3].Points.AddXY(X++, datos.practica4);
            chGrafica2.Update();
        }
        //Método que funciona cuando se presionan las teclas indicadas
        private void frmCuatro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Right)
            {
                A++;
                chGrafica.Series[1].Points.AddXY(X++, A);
                chGrafica.Series[0].Points.AddXY(X++, Y);
                chGrafica.Update();
            }
            if (e.KeyChar == (char)Keys.Left)
            {
                B++;
                chGrafica.Series[2].Points.AddXY(X++, B);
                chGrafica.Series[0].Points.AddXY(X++, Y);
                chGrafica.Update();
            }
        }
        //Se agrega un elemento a la grafica dos cuando se mueve el mouse
        private void frmCuatro_MouseMove(object sender, MouseEventArgs e)
        {
            //Se asigna a la variable la posicion del mouse
            vY = e.Y;
            vX = e.X;
            chMousem.Series[0].Points.AddXY(X++, vY);
        }
        //Método que registra los clicks del mouse en la primera grafica
        private void frmCuatro_Click(object sender, EventArgs e)
        {
            chGrafica.Series[0].Points.AddXY(X++, Y++);
            chGrafica.Update();
        }
        //Método que asigna un valor en Y al movimiento del mouse
        private void tmrMouse_Tick(object sender, EventArgs e)
        {
            if (chMousem.Series[0].Points.Count > 10)
            {
                chMousem.Series[1].Points.RemoveAt(0);
                chMousem.Update();
            }
        }
    }
}
