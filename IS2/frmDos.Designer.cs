﻿namespace IS2
{
    partial class frmDos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrPrincipal = new System.Windows.Forms.Timer(this.components);
            this.pbDos = new System.Windows.Forms.PictureBox();
            this.pbUno = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbDos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUno)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrPrincipal
            // 
            this.tmrPrincipal.Enabled = true;
            this.tmrPrincipal.Interval = 20;
            this.tmrPrincipal.Tick += new System.EventHandler(this.tmrPrincipal_Tick);
            // 
            // pbDos
            // 
            this.pbDos.Image = global::IS2.Properties.Resources._9;
            this.pbDos.Location = new System.Drawing.Point(560, 12);
            this.pbDos.Name = "pbDos";
            this.pbDos.Size = new System.Drawing.Size(128, 128);
            this.pbDos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbDos.TabIndex = 1;
            this.pbDos.TabStop = false;
            // 
            // pbUno
            // 
            this.pbUno.Image = global::IS2.Properties.Resources._3;
            this.pbUno.Location = new System.Drawing.Point(138, 214);
            this.pbUno.Name = "pbUno";
            this.pbUno.Size = new System.Drawing.Size(128, 128);
            this.pbUno.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbUno.TabIndex = 0;
            this.pbUno.TabStop = false;
            // 
            // frmDos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(922, 593);
            this.Controls.Add(this.pbDos);
            this.Controls.Add(this.pbUno);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmDos";
            this.Opacity = 0.6D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmDos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmDos_Load);
            this.Click += new System.EventHandler(this.frmDos_Click);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmDos_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pbDos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUno)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbUno;
        private System.Windows.Forms.PictureBox pbDos;
        private System.Windows.Forms.Timer tmrPrincipal;
    }
}