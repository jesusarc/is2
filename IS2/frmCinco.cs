﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmCinco : Form
    {
        public frmCinco()
        {
            InitializeComponent();
        }
        //Se cierra la forma y se regresa al menú principal
        private void lblSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 menu = new Form1();
            menu.Show();
        }

        private void frmCinco_Load(object sender, EventArgs e)
        {
           
        }
        //Boton de busqueda de archivos
        //Se crean las variables con las que se leeran los nombres de las canciones.
        string[] nombre, ruta;

        //Botón que inicia la canción seleccionada.
        //string cancion;
        private void pbPlay_Click(object sender, EventArgs e)
        {
            if (lstPlaylist.Items.Count > 1)
            {
                //player.URL = ruta[lstPlaylist.SelectedIndex];
                //player.Ctlcontrols.play();
                tmrCancion.Start();
            }
            
        }
        //Método que funciona para detener la reproducción
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (lstPlaylist.Items.Count > 1)
            {
                //player.Ctlcontrols.stop();
            }
            
        }
        //Se cambia a la siguiente pista
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (lstPlaylist.Items.Count >= 1)
            {
                lstPlaylist.SelectedIndex++;
            }
            
        }
        //Se cambia a la pista anterior
        private void pictureBox5_Click(object sender, EventArgs e)
        {
            if (lstPlaylist.Items.Count >1)
            {
                    lstPlaylist.SelectedIndex--; 
            }
            
        }
        //Método que funciona para eliminar canción del playlist
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (lstPlaylist.Items.Count >= 1)
            {
                lstPlaylist.Items.RemoveAt(lstPlaylist.SelectedIndex);
            }
            
        }

        private void frmCinco_KeyPress(object sender, KeyPressEventArgs e)
        {
            

        }
        //Se cambia el volumen de la aplicación
        private void trbVolumen_Scroll(object sender, EventArgs e)
        {
            //player.settings.volume = trbVolumen.Value;
        }

        private void tmrCancion_Tick(object sender, EventArgs e)
        {
            
        }

        //Metodo que funciona para realizar la busqueda de archivos.
        private void pbBuscar_Click(object sender, EventArgs e)
        {
            //Se inicia la selección multiple.
            pfdSeleccion.Filter = "MP3 Files(*.mp3)|*.mp3";
            pfdSeleccion.Multiselect = true;
            if (pfdSeleccion.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //Cuando se seleccionan los archivos, estos se agregan a la lista de playlist.
                nombre = pfdSeleccion.SafeFileNames;
                ruta = pfdSeleccion.FileNames;
                for (int i = 0; i < ruta.Length; i++)
                {
                    lstPlaylist.Items.Add(nombre[i]);
                }
            }
        }
    }
}
