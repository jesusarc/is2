﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmTres : Form
    {
        public frmTres()
        {
            InitializeComponent();
        }

        private void frmTres_Load(object sender, EventArgs e)
        {
            frmTres tres = new frmTres();
            tres.Opacity = .80;
        }
        //Se cierra la forma y se manda llamar el menu principal
        private void lblSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 menu = new Form1();
            menu.Show();
        }
        //Se manda llamar el juego
        private void label2_Click(object sender, EventArgs e)
        {
            frmJuego juego = new frmJuego();
            juego.Show();
            this.Hide();
        }
    }
}
