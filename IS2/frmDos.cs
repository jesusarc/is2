﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmDos : Form
    {
        //Variables globales.
        public static int a = 5;
        public static int b = 4;
        public static int c = 5;
        public static int d = 4;
        //Se obtiene el tamaño de la pantalla
        public static int alto = Screen.PrimaryScreen.Bounds.Height;
        public static int ancho = Screen.PrimaryScreen.Bounds.Width;
        //Método que funciona cuando se presiona la tecla Escape.
        private void frmDos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
                Form1 menu = new Form1();
                menu.Show();
            }
        }

        private void frmDos_Load(object sender, EventArgs e)
        {
            tmrPrincipal.Start();
        }
        //Método que funciona durante el timer
        private void tmrPrincipal_Tick(object sender, EventArgs e)
        {
            //Movimiento de la primera estación espacial.
            if (pbUno.Location.Y <= 0 || (pbUno.Location.Y >= alto - pbUno.Height))
            {
                a = a * -1;
            }
            if (pbUno.Location.X <= 0 || (pbUno.Location.X >= ancho - pbUno.Width))
            {
                b = b * -1;
            }
                pbUno.Location = new Point(pbUno.Location.X + b, pbUno.Location.Y + a);
            //Movimiento de la segunda estación espacial.
            if (pbDos.Location.Y <= 0 || (pbDos.Location.Y >= alto - pbDos.Height))
            {
                c = c * -1;
            }
            if (pbDos.Location.X <= 0 || (pbDos.Location.X >= ancho - pbDos.Width))
            {
                d = d * -1;
            }
                pbDos.Location = new Point(pbDos.Location.X + d, pbDos.Location.Y + c);
            //Choques espaciales
            if (pbUno.Bounds.IntersectsWith(pbDos.Bounds))
            {
                a = a * -1;
                b = b * -1;
                c = c * -1;
                d = d * -1;
            }

        }

        private void frmDos_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 menu = new Form1();
            menu.Show();
        }
        //Método que funciona al hacer click sobre el panel
        public frmDos()
        {
            InitializeComponent();
            // El formulario recibirá cualquier evento antes que sus controles
            this.KeyPreview = true;
            // El formulario, siempre en primer plano
            this.TopLevel = true;
        }
    }
}
