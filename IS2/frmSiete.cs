﻿using System;
using System.Windows.Forms;
using System.Net.Sockets; //Libreria usada para realizar conexiones TCP/IP
using System.Drawing; //Libreria que permite dibujar
using System.Runtime.Serialization.Formatters.Binary; //Libreria para conversion a datos binarios

namespace IS2
{
    public partial class frmSiete : Form
    {
        /// <summary>
        /// Declaración de variables
        /// </summary>
        private readonly TcpClient cliente = new TcpClient();
        private NetworkStream transmision;
        private int puerto;
        //Método en el que se captura la posicion del mouse en x
        private static int movimientoMouseX()
        {
            int x = Cursor.Position.X;
                return x;
        }
        //Método en el que se captura la posición del mouse en y
        private static int movimientoMouseY()
        {
            int y = Cursor.Position.Y;
            return y;
        }
        //Método que captura la posicion del cursor
        private static Point posicionMouse()
        {
            Point punto = new Point(movimientoMouseX(), movimientoMouseY());
            return punto;
        }
        private void enviarMouse()
        {
            //Se convierte al imagen del escritorio a binario
            //Ya que solo pueden enviarse este tipo de datos por la red
            BinaryFormatter formatear = new BinaryFormatter();
            transmision = cliente.GetStream();
            formatear.Serialize(transmision, posicionMouse());
        }
        public frmSiete()
        {
            InitializeComponent();
        }
        //Se cierra la práctica y se regresa al menu principal
        private void lblSalir_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Close();
        }
        //Método en el que se realiza la conexión al equipo a controlar
        private void btnConectar_Click(object sender, EventArgs e)
        {
            //Puerto en el que se realizara la conexión
            puerto = 2015;
            //Manejo de excepciones
            try
            {
                //Se crea la conexión
                cliente.Connect(txtIP.Text, puerto);
                MessageBox.Show("Conexión Establecida");
            }catch(Exception ex)
            {
                //Mensaje en caso de que la conexión no pueda realizarse
                MessageBox.Show("Ocurrio un error: \n" + ex.Message);
            }
        }
        //Método que ocurre al dar click en el boton de controlar
        private void btnControlar_Click(object sender, EventArgs e)
        {
            if (btnControlar.Text == "Controlar")
            {
                tmrMouse.Start();
                btnControlar.Text = "Controlando";
            }
            else
            {
                tmrMouse.Stop();
                btnControlar.Text = "Controlar";
            }
        }
        //Método que ejecuta el envio de posicion del mouse utilizando un timer
        private void tmrMouse_Tick(object sender, EventArgs e)
        {
            enviarMouse();
        }

        private void frmSiete_Load(object sender, EventArgs e)
        {

        }
    }
}
