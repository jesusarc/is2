﻿using System.Windows.Forms;
using System.Net.Sockets; //Libreria usada para realizar conexiones TCP/IP
using System.Threading; //Libreria para crear hilos entre aplicaciones.
using System.Runtime.Serialization.Formatters.Binary; //Libreria para conversion a datos binarios
using System.Drawing; //Libreria que permite dibujar
using System.Net; //Libreria que permite la conexion web

namespace IS2
{
    public partial class frmRemoto : Form
    {
        /// <summary>
        /// Declaración de Variables
        /// </summary>
        private readonly int puerto;
        private TcpClient cliente;   //Variable que recibe el puerto del cliente
        private TcpListener servidor;//Variable del puerto servidor
        private NetworkStream transmision; //Variable para realizar el envio de datos
        //Variables para la creación de hilo entre ambas aplicaciones.
        private readonly Thread recibiendo; 
        private readonly Thread obtenerImagen;
        public frmRemoto(int Puerto)
        {
            //Se iguala el valor de la variable creada al valor que será recibido
            puerto = Puerto;
            cliente = new TcpClient();
            recibiendo = new Thread(comenzarRecepcion);
            obtenerImagen = new Thread(recibirImagen);
            InitializeComponent();
        }
        //Método en el que se inicia con la recepcion de datos.
        private void comenzarRecepcion()
        {
            //Mientras el cliente no este conectado se inicia el servidor 
            while (!cliente.Connected)
            {
                servidor.Start();
                cliente = servidor.AcceptTcpClient();
            }
            //Se obtiene la imagen a mostrar.
            obtenerImagen.Start();
        }
        //Método que detiene la recepcion de datos.
        private void detenerRecepcion()
        {
            //Se detiene el servidor y el cliente se borra.
            servidor.Stop();
            cliente = null;
            //Si existe la conexion y muestra de imagen se termina con ambas.
            if (recibiendo.IsAlive) recibiendo.Abort();
            if (obtenerImagen.IsAlive) obtenerImagen.Abort();
        }
        //Método en el que se reciben los datos de imagen.
        private void recibirImagen()
        {
            //Se transforman los datos binarios recibidos a imagen para poder mostrar el escritorio.
            BinaryFormatter formatear = new BinaryFormatter();
            while(cliente.Connected)
            {
                transmision = cliente.GetStream();
                pbImagenRemota.Image = (Image) formatear.Deserialize(transmision);
            }
        }
        //Método que se ejecuta al cargar la forma
        private void frmRemoto_Load(object sender, System.EventArgs e)
        {
            servidor = new TcpListener(IPAddress.Any, puerto);
            recibiendo.Start();
        }
        //Método que se ejecuta al cerrar la ventana
        private void frmRemoto_FormClosing(object sender, FormClosingEventArgs e)
        {
            detenerRecepcion();
        }
    }
}
