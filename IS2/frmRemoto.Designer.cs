﻿namespace IS2
{
    partial class frmRemoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbImagenRemota = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenRemota)).BeginInit();
            this.SuspendLayout();
            // 
            // pbImagenRemota
            // 
            this.pbImagenRemota.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pbImagenRemota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbImagenRemota.Location = new System.Drawing.Point(0, 0);
            this.pbImagenRemota.Name = "pbImagenRemota";
            this.pbImagenRemota.Size = new System.Drawing.Size(445, 280);
            this.pbImagenRemota.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagenRemota.TabIndex = 0;
            this.pbImagenRemota.TabStop = false;
            // 
            // frmRemoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 280);
            this.Controls.Add(this.pbImagenRemota);
            this.Name = "frmRemoto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Escritorio a Distancia";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRemoto_FormClosing);
            this.Load += new System.EventHandler(this.frmRemoto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenRemota)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImagenRemota;
    }
}