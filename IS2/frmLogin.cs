﻿using System;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        //Al dar click en el boton de login se hacen las comprobaciones de usuario y contraseña
        private void btnLogin_Click(object sender, EventArgs e)
        {
            //Se construye la forma Splash
            frmSplash splash = new frmSplash();
            //se comienza con las validaciones
            if (txtUsuario.Text == datos.user1)
            {
                //Si el usuario es correcto se valida el password
                if (txtPassword.Text == datos.pass1)
                {
                    splash.Show();
                    this.Hide();
                }
                //Si no se ingresa el password en base al usuario en el cual se encuentra la
                //condicion del password se envia el siguiente mensaje.
                else MessageBox.Show("Contraseña Incorrecta");
            }
            else if (txtUsuario.Text == datos.user2)
            {
                if(txtPassword.Text == datos.pass2)
                {
                    splash.Show();
                    this.Hide();
                }
                else MessageBox.Show("Contraseña Incorrecta");
            }
            else if (txtUsuario.Text == datos.user3)
            {
                if (txtPassword.Text == datos.pass3)
                {
                    splash.Show();
                    this.Hide();
                }
                else MessageBox.Show("Contraseña Incorrecta");
            }
            else MessageBox.Show("Usuario incorrecto");
        }
        //Se establece el fondo del titulo de la forma transparente en base al fondo.
        private void frmLogin_Load(object sender, EventArgs e)
        {
            lblTitulo.Parent = pbFondo;
        }
    }
}
