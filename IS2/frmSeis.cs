﻿using System;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;

namespace IS2
{
    public partial class frmSeis : Form
    {
        
        public frmSeis()
        {
            InitializeComponent();
        }
        //Se cierra esta forma y se manda llamar al menú principal
        private void lblSalir_Click(object sender, EventArgs e)
        {
            Form1 menu = new Form1();
            menu.Show();
            this.Close();
        }
        //Método que funciona al momento de dar click en el boton de nuevo prestamo.
        private void btnPrestamo_Click(object sender, EventArgs e)
        {
            RegistroPrestamo prestamo = new RegistroPrestamo();
            prestamo.Show();
        }

        private void frmSeis_Load(object sender, EventArgs e)
        {
            Llenar_tablas();

            
            cbPrestamistas.Items.Add("Jesus");
            cbPrestamistas.Items.Add("Karen");

            cbIva.Items.Add("5");
            cbIva.Items.Add("10");
            cbIva.Items.Add("15");
            cbIva.Items.Add("20");

            cbDedudor.Items.Add("Isis");
            cbDedudor.Items.Add("Raquel");
            cbDedudor.Items.Add("Ivan");
            cbDedudor.Items.Add("Melissa");
            cbDedudor.Items.Add("Omar");
        }

        private void Llenar_tablas()
        {
            //Se llena el dgv de prestamistas
            dgvPrestamistas.Rows.Add("Jesus", datos.interes_prestamista_chuy[0], datos.interes_prestamista_chuy[1], datos.interes_prestamista_chuy[2], datos.interes_prestamista_chuy[3], datos.interes_prestamista_chuy[4], datos.interes_prestamista_chuy[5], datos.interes_prestamista_chuy[6], datos.interes_prestamista_chuy[7], datos.interes_prestamista_chuy[8], datos.interes_prestamista_chuy[9], datos.interes_prestamista_chuy[10], datos.interes_prestamista_chuy[11]);
            dgvPrestamistas.Rows.Add("Karen", datos.interes_prestamista_karen[0], datos.interes_prestamista_karen[1], datos.interes_prestamista_karen[2], datos.interes_prestamista_karen[3], datos.interes_prestamista_karen[4], datos.interes_prestamista_karen[5], datos.interes_prestamista_karen[6], datos.interes_prestamista_karen[7], datos.interes_prestamista_karen[8], datos.interes_prestamista_karen[9], datos.interes_prestamista_karen[10], datos.interes_prestamista_karen[11]);

            //Se llena el dgv de deudores
            dgvDeudores.Rows.Add("Isis", datos.interes_deudor_isis[0], datos.interes_deudor_isis[1], datos.interes_deudor_isis[2], datos.interes_deudor_isis[3], datos.interes_deudor_isis[4], datos.interes_deudor_isis[5], datos.interes_deudor_isis[6], datos.interes_deudor_isis[7], datos.interes_deudor_isis[8], datos.interes_deudor_isis[9], datos.interes_deudor_isis[10], datos.interes_deudor_isis[11]);
            dgvDeudores.Rows.Add("Raquel", datos.interes_deudor_raquel[0], datos.interes_deudor_raquel[1], datos.interes_deudor_raquel[2], datos.interes_deudor_raquel[3], datos.interes_deudor_raquel[4], datos.interes_deudor_raquel[5], datos.interes_deudor_raquel[6], datos.interes_deudor_raquel[7], datos.interes_deudor_raquel[8], datos.interes_deudor_raquel[9], datos.interes_deudor_raquel[10], datos.interes_deudor_raquel[11]);
            dgvDeudores.Rows.Add("Ivan", datos.interes_deudor_ivan[0], datos.interes_deudor_ivan[1], datos.interes_deudor_ivan[2], datos.interes_deudor_ivan[3], datos.interes_deudor_ivan[4], datos.interes_deudor_ivan[5], datos.interes_deudor_ivan[6], datos.interes_deudor_ivan[7], datos.interes_deudor_ivan[8], datos.interes_deudor_ivan[9], datos.interes_deudor_ivan[10], datos.interes_deudor_ivan[11]);
            dgvDeudores.Rows.Add("Melissa", datos.interes_deudor_melissa[0], datos.interes_deudor_melissa[1], datos.interes_deudor_melissa[2], datos.interes_deudor_melissa[3], datos.interes_deudor_melissa[4], datos.interes_deudor_melissa[5], datos.interes_deudor_melissa[6], datos.interes_deudor_melissa[7], datos.interes_deudor_melissa[8], datos.interes_deudor_melissa[9], datos.interes_deudor_melissa[10], datos.interes_deudor_melissa[11]);
            dgvDeudores.Rows.Add("Omar", datos.interes_deudor_omar[0], datos.interes_deudor_omar[1], datos.interes_deudor_omar[2], datos.interes_deudor_omar[3], datos.interes_deudor_omar[4], datos.interes_deudor_omar[5], datos.interes_deudor_omar[6], datos.interes_deudor_omar[7], datos.interes_deudor_omar[8], datos.interes_deudor_omar[9], datos.interes_deudor_omar[10], datos.interes_deudor_omar[11]);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtCantidad.Text == "")
            {
                MessageBox.Show("Llena todos los campos");
            }
            else
            {
                
                
                if (cbPrestamistas.Text == "Jesus")
                {
                    switch (cbDedudor.Text)
                    {
                        case "Isis":
                            int mes = dtFecha.Value.Month;
                            int cantidad = Convert.ToInt32(txtCantidad.Text);
                            double val = .01;
                            double interes = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val;
                            datos.interes_prestamista_chuy[mes] = datos.interes_prestamista_chuy[mes] + (cantidad * interes);
                            datos.interes_deudor_isis[mes] = datos.interes_deudor_isis[mes] + (cantidad * interes);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Isis", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedItem.ToString(), "Jesus", 0, cantidad);
                            break;
                        case "Raquel":
                            int mes2 = dtFecha.Value.Month;
                            int cantidad2 = Convert.ToInt32(txtCantidad.Text);
                            double val2 = .01;
                            double interes2 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val2;
                            datos.interes_prestamista_chuy[mes2] = datos.interes_prestamista_chuy[mes2] + (cantidad2 * interes2);
                            datos.interes_deudor_raquel[mes2] = datos.interes_deudor_raquel[mes2] + (cantidad2 * interes2);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Raquel", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Jesus", 0, cantidad2);
                            break;
                        case "Ivan":
                            int mes3 = dtFecha.Value.Month;
                            int cantidad3 = Convert.ToInt32(txtCantidad.Text);
                            double val3 = .01;
                            double interes3 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val3;
                            datos.interes_prestamista_chuy[mes3] = datos.interes_prestamista_chuy[mes3] + (cantidad3 * interes3);
                            datos.interes_deudor_ivan[mes3] = datos.interes_deudor_ivan[mes3] + (cantidad3 * interes3);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Ivan", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Jesus", 0, cantidad3);
                            break;
                        case "Melissa":
                            int mes4 = dtFecha.Value.Month;
                            int cantidad4 = Convert.ToInt32(txtCantidad.Text);
                            double val4 = .01;
                            double interes4 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val4;
                            datos.interes_prestamista_chuy[mes4] = datos.interes_prestamista_chuy[mes4] + (cantidad4 * interes4);
                            datos.interes_deudor_melissa[mes4] = datos.interes_deudor_melissa[mes4] + (cantidad4 * interes4);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Melissa", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Jesus", 0, cantidad4);
                            break;
                        case "Omar":
                            int mes5 = dtFecha.Value.Month;
                            int cantidad5 = Convert.ToInt32(txtCantidad.Text);
                            double val5 = .01;
                            double interes5 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val5;
                            datos.interes_prestamista_chuy[mes5] = datos.interes_prestamista_chuy[mes5] + (cantidad5 * interes5);
                            datos.interes_deudor_omar[mes5] = datos.interes_deudor_omar[mes5] + (cantidad5 * interes5);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Omar", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Jesus", 0, cantidad5);
                            break;
                    }
                }
                else if (cbPrestamistas.Text == "Karen")
                {
                    switch (cbDedudor.Text)
                    {
                        case "Isis":
                            int mes6 = dtFecha.Value.Month;
                            int cantidad6 = Convert.ToInt32(txtCantidad.Text);
                            double val6 = .01;
                            double interes6 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val6;
                            datos.interes_prestamista_karen[mes6] = datos.interes_prestamista_chuy[mes6] + (cantidad6 * interes6);
                            datos.interes_deudor_isis[mes6] = datos.interes_deudor_isis[mes6] + (cantidad6 * interes6);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Isis", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Karen", 0, cantidad6);
                            break;
                        case "Raquel":
                            int mes7 = dtFecha.Value.Month;
                            int cantidad7 = Convert.ToInt32(txtCantidad.Text);
                            double val7 = .01;
                            double interes7 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val7;
                            datos.interes_prestamista_karen[mes7] = datos.interes_prestamista_chuy[mes7] + (cantidad7 * interes7);
                            datos.interes_deudor_raquel[mes7] = datos.interes_deudor_raquel[mes7] + (cantidad7 * interes7);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Raquel", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Karen", 0, cantidad7);
                            break;
                        case "Ivan":
                            int mes8 = dtFecha.Value.Month;
                            int cantidad8 = Convert.ToInt32(txtCantidad.Text);
                            double val8 = .01;
                            double interes8 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val8;
                            datos.interes_prestamista_karen[mes8] = datos.interes_prestamista_chuy[mes8] + (cantidad8 * interes8);
                            datos.interes_deudor_ivan[mes8] = datos.interes_deudor_ivan[mes8] + (cantidad8 * interes8);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Ivan", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Karen", 0, cantidad8);
                            break;
                        case "Melissa":
                            int mes9 = dtFecha.Value.Month;
                            int cantidad9 = Convert.ToInt32(txtCantidad.Text);
                            double val9 = .01;
                            double interes9 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val9;
                            datos.interes_prestamista_karen[mes9] = datos.interes_prestamista_chuy[mes9] + (cantidad9 * interes9);
                            datos.interes_deudor_melissa[mes9] = datos.interes_deudor_melissa[mes9] + (cantidad9 * interes9);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Melissa", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Karen", 0, cantidad9);
                            break;
                        case "Omar":
                            int mes10 = dtFecha.Value.Month;
                            int cantidad10 = Convert.ToInt32(txtCantidad.Text);
                            double val10 = .01;
                            double interes10 = Convert.ToInt32(cbIva.SelectedItem.ToString()) * val10;
                            datos.interes_prestamista_karen[mes10] = datos.interes_prestamista_chuy[mes10] + (cantidad10 * interes10);
                            datos.interes_deudor_omar[mes10] = datos.interes_deudor_omar[mes10] + (cantidad10 * interes10);
                            dgvPrestamistas.Rows.Clear();
                            dgvDeudores.Rows.Clear();
                            Llenar_tablas();
                            dgvPrestamos.Rows.Add("Omar", txtCantidad.Text, dtFecha.Value.ToString(), cbIva.SelectedIndex.ToString(), "Karen", 0, cantidad10);
                            break;
                    }
                }
            }
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*";
            if (save.ShowDialog() == DialogResult.OK)
            {
                string filename = save.FileName;
                DateTime hora = DateTime.Now;
                string fecha = Convert.ToDateTime(hora).Day + "/" + Convert.ToDateTime(hora).Month + "/" + Convert.ToDateTime(hora).Year;
                Document doc = new Document(PageSize.A3, 9, 9, 9, 9);
                Chunk titulo = new Chunk("Consecutivo Prestamos - Deudores  " + fecha, FontFactory.GetFont("ARIAL", 18)); //Titulo del documento
                Chunk prestamista = new Chunk(lblPrestamistas.Text + " ", FontFactory.GetFont("COURIER", 18));
                Chunk deudores = new Chunk(lblDeudores.Text, FontFactory.GetFont("COURIER", 18));
                Chunk prestamos = new Chunk(lblPrestamos.Text, FontFactory.GetFont("COURIER", 18));
                Paragraph salto = new Paragraph(" "); //Salto de línea
                try
                {
                    FileStream file = new FileStream(filename, FileMode.OpenOrCreate); //Se crea el archivo
                    PdfWriter writer = PdfWriter.GetInstance(doc, file); //Se escribe el archivo
                    writer.ViewerPreferences = PdfWriter.PageModeUseThumbs;
                    writer.ViewerPreferences = PdfWriter.PageLayoutOneColumn;
                    doc.Open();
                    doc.Add(salto);
                    doc.Add(new Paragraph(titulo));
                    doc.Add(salto);
                    doc.Add(new Paragraph(prestamista));
                    doc.Add(salto);
                    Prestamistas(doc);
                    doc.Add(salto);
                    doc.Add(new Paragraph(deudores));
                    doc.Add(salto);
                    Deudores(doc);
                    doc.Add(salto);
                    doc.Add(new Paragraph(prestamos));
                    doc.Add(salto);
                    Prestamos_tb(doc);
                    doc.Add(salto);
                    Process.Start(filename);
                    doc.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        //Se crean las tablas
        private void Prestamistas(Document document) 
        {
            //se crea un objeto PdfTable con el numero de columnas del dataGridView de prestamistas
            PdfPTable datatable = new PdfPTable(dgvPrestamistas.ColumnCount);
            //Configuración de Diseño del PDF
            datatable.DefaultCell.Padding = 2;
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 1;

            datatable.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.WHITE;
            datatable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.BLACK;
            iTextSharp.text.Font fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.COURIER);

            Phrase objP = new Phrase("A", fuente);

            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

            //Se generan los titulos

            for (int i = 0; i < dgvPrestamistas.ColumnCount; i++)
            {

                objP = new Phrase(dgvPrestamistas.Columns[i].HeaderText, fuente);
                datatable.HorizontalAlignment = Element.ALIGN_CENTER;

                datatable.AddCell(objP);

            }
            datatable.HeaderRows = 2;

            datatable.DefaultCell.BorderWidth = 1;

            //Se crea el pdf
            for (int i = 0; i < dgvPrestamistas.RowCount; i++)
            {
                for (int j = 0; j < dgvPrestamistas.ColumnCount; j++)
                {
                    if (dgvPrestamistas[j, i].Value != null)
                    {
                        objP = new Phrase(dgvPrestamistas[j, i].Value.ToString(), fuente);
                        datatable.AddCell(objP);
                    }
                }
                datatable.CompleteRow();
            }
            document.Add(datatable);
        }

        private void Deudores(Document document)
        {
            //se crea un objeto PdfTable con el numero de columnas del dataGridView de deudores
            PdfPTable datatable = new PdfPTable(dgvDeudores.ColumnCount);
            //Configuración de Diseño del PDF
            datatable.DefaultCell.Padding = 2;
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 1;

            datatable.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.WHITE;
            datatable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.BLACK;
            iTextSharp.text.Font fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.COURIER);

            Phrase objP = new Phrase("A", fuente);

            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

            //Se generan los titulos

            for (int i = 0; i < dgvDeudores.ColumnCount; i++)
            {

                objP = new Phrase(dgvDeudores.Columns[i].HeaderText, fuente);
                datatable.HorizontalAlignment = Element.ALIGN_CENTER;

                datatable.AddCell(objP);

            }
            datatable.HeaderRows = 2;

            datatable.DefaultCell.BorderWidth = 1;

            //Se crea el pdf
            for (int i = 0; i < dgvDeudores.RowCount; i++)
            {
                for (int j = 0; j < dgvDeudores.ColumnCount; j++)
                {
                    if (dgvDeudores[j, i].Value != null)
                    {
                        objP = new Phrase(dgvDeudores[j, i].Value.ToString(), fuente);
                        datatable.AddCell(objP);
                    }
                }
                datatable.CompleteRow();
            }
            document.Add(datatable);
        }

        private void Prestamos_tb(Document document)
        {
            //se crea un objeto PdfTable con el numero de columnas del dataGridView de prestamos
            PdfPTable datatable = new PdfPTable(dgvPrestamos.ColumnCount);
            //Configuración de Diseño del PDF
            datatable.DefaultCell.Padding = 2;
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 1;

            datatable.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.WHITE;
            datatable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.BLACK;
            iTextSharp.text.Font fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.COURIER);

            Phrase objP = new Phrase("A", fuente);

            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

            //Se generan los titulos

            for (int i = 0; i < dgvPrestamos.ColumnCount; i++)
            {

                objP = new Phrase(dgvPrestamos.Columns[i].HeaderText, fuente);
                datatable.HorizontalAlignment = Element.ALIGN_CENTER;

                datatable.AddCell(objP);

            }
            datatable.HeaderRows = 2;

            datatable.DefaultCell.BorderWidth = 1;

            //Se crea el pdf
            for (int i = 0; i < dgvPrestamos.RowCount; i++)
            {
                for (int j = 0; j < dgvPrestamos.ColumnCount; j++)
                {
                    if (dgvPrestamos[j, i].Value != null)
                    {
                        objP = new Phrase(dgvPrestamos[j, i].Value.ToString(), fuente);
                        datatable.AddCell(objP);
                    }
                }
                datatable.CompleteRow();
            }
            document.Add(datatable);
        }

    }
}
