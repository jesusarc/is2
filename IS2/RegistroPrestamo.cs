﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS2
{
    public partial class RegistroPrestamo : Form
    {
        public RegistroPrestamo()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if(cbPrestamistas.Text == "Jesus")
            {
                switch (cbDedudor.Text)
                {
                    case "Isis": datos.interes_prestamista_chuy[1] = 100;
                        break;
                }
            }
            else if (cbPrestamistas.Text == "Karen")
            {

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void RegistroPrestamo_Load(object sender, EventArgs e)
        {
            cbPrestamistas.Items.Add("Jesus");
            cbPrestamistas.Items.Add("Karen");
            cbPrestamistas.Items.Add("Rodrigo");
            cbPrestamistas.Items.Add("Lorena");
            cbPrestamistas.Items.Add("Pepe");

            cbIva.Items.Add("5%");
            cbIva.Items.Add("10%");
            cbIva.Items.Add("15%");
            cbIva.Items.Add("20%");

            cbDedudor.Items.Add("Isis");
            cbDedudor.Items.Add("Raquel");
            cbDedudor.Items.Add("Ivan");
            cbDedudor.Items.Add("Melissa");
            cbDedudor.Items.Add("Omar");
        }
    }
}
