﻿namespace IS2
{
    partial class frmCuatro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chGrafica = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSalir = new System.Windows.Forms.Label();
            this.chGrafica2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.lblPracticas = new System.Windows.Forms.Label();
            this.tmrMouse = new System.Windows.Forms.Timer(this.components);
            this.chMousem = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chGrafica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chGrafica2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chMousem)).BeginInit();
            this.SuspendLayout();
            // 
            // chGrafica
            // 
            this.chGrafica.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chGrafica.BorderlineColor = System.Drawing.Color.Maroon;
            chartArea7.BackColor = System.Drawing.Color.WhiteSmoke;
            chartArea7.Name = "chartTeclas";
            this.chGrafica.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.chGrafica.Legends.Add(legend7);
            this.chGrafica.Location = new System.Drawing.Point(12, 77);
            this.chGrafica.Name = "chGrafica";
            this.chGrafica.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series17.BackSecondaryColor = System.Drawing.Color.White;
            series17.BorderColor = System.Drawing.Color.White;
            series17.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            series17.ChartArea = "chartTeclas";
            series17.Color = System.Drawing.Color.Yellow;
            series17.LabelBackColor = System.Drawing.Color.WhiteSmoke;
            series17.LabelBorderColor = System.Drawing.Color.WhiteSmoke;
            series17.Legend = "Legend1";
            series17.MarkerColor = System.Drawing.Color.WhiteSmoke;
            series17.Name = "Clicks Mouse";
            series18.ChartArea = "chartTeclas";
            series18.LabelBackColor = System.Drawing.Color.WhiteSmoke;
            series18.Legend = "Legend1";
            series18.Name = "Tecla : Derecha";
            series19.ChartArea = "chartTeclas";
            series19.LabelBackColor = System.Drawing.Color.WhiteSmoke;
            series19.Legend = "Legend1";
            series19.Name = "Tecla: Izquierda";
            this.chGrafica.Series.Add(series17);
            this.chGrafica.Series.Add(series18);
            this.chGrafica.Series.Add(series19);
            this.chGrafica.Size = new System.Drawing.Size(545, 280);
            this.chGrafica.TabIndex = 0;
            this.chGrafica.Text = "chart1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(361, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Práctica IV - Gráficas";
            // 
            // lblSalir
            // 
            this.lblSalir.AutoSize = true;
            this.lblSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSalir.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalir.ForeColor = System.Drawing.Color.Teal;
            this.lblSalir.Location = new System.Drawing.Point(12, 559);
            this.lblSalir.Name = "lblSalir";
            this.lblSalir.Size = new System.Drawing.Size(54, 25);
            this.lblSalir.TabIndex = 3;
            this.lblSalir.Text = "Salir";
            this.lblSalir.Click += new System.EventHandler(this.lblSalir_Click);
            // 
            // chGrafica2
            // 
            this.chGrafica2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chGrafica2.BorderlineColor = System.Drawing.Color.Maroon;
            chartArea8.Name = "ChartArea1";
            this.chGrafica2.ChartAreas.Add(chartArea8);
            legend8.Name = "Legend1";
            this.chGrafica2.Legends.Add(legend8);
            this.chGrafica2.Location = new System.Drawing.Point(380, 304);
            this.chGrafica2.Name = "chGrafica2";
            this.chGrafica2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series20.ChartArea = "ChartArea1";
            series20.Legend = "Legend1";
            series20.Name = "Práctica Uno";
            series21.ChartArea = "ChartArea1";
            series21.Legend = "Legend1";
            series21.Name = "Práctica Dos";
            series22.ChartArea = "ChartArea1";
            series22.Legend = "Legend1";
            series22.Name = "Práctica Tres";
            series23.ChartArea = "ChartArea1";
            series23.Legend = "Legend1";
            series23.Name = "Práctica Cuatro";
            this.chGrafica2.Series.Add(series20);
            this.chGrafica2.Series.Add(series21);
            this.chGrafica2.Series.Add(series22);
            this.chGrafica2.Series.Add(series23);
            this.chGrafica2.Size = new System.Drawing.Size(515, 280);
            this.chGrafica2.TabIndex = 4;
            this.chGrafica2.Text = "chart1";
            // 
            // lblRegistros
            // 
            this.lblRegistros.AutoSize = true;
            this.lblRegistros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRegistros.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistros.ForeColor = System.Drawing.Color.Teal;
            this.lblRegistros.Location = new System.Drawing.Point(563, 77);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(211, 25);
            this.lblRegistros.TabIndex = 5;
            this.lblRegistros.Text = "Régistros del Sistema";
            // 
            // lblPracticas
            // 
            this.lblPracticas.AutoSize = true;
            this.lblPracticas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPracticas.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPracticas.ForeColor = System.Drawing.Color.Teal;
            this.lblPracticas.Location = new System.Drawing.Point(186, 360);
            this.lblPracticas.Name = "lblPracticas";
            this.lblPracticas.Size = new System.Drawing.Size(188, 25);
            this.lblPracticas.TabIndex = 6;
            this.lblPracticas.Text = "Registros Practicas";
            // 
            // tmrMouse
            // 
            this.tmrMouse.Tick += new System.EventHandler(this.tmrMouse_Tick);
            // 
            // chMousem
            // 
            this.chMousem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chMousem.BorderlineColor = System.Drawing.Color.Maroon;
            chartArea9.BackColor = System.Drawing.Color.WhiteSmoke;
            chartArea9.Name = "chartTeclas";
            this.chMousem.ChartAreas.Add(chartArea9);
            legend9.Name = "Legend1";
            this.chMousem.Legends.Add(legend9);
            this.chMousem.Location = new System.Drawing.Point(534, 120);
            this.chMousem.Name = "chMousem";
            this.chMousem.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series24.ChartArea = "chartTeclas";
            series24.LabelBackColor = System.Drawing.Color.WhiteSmoke;
            series24.Legend = "Legend1";
            series24.Name = "Movimiento Mouse";
            this.chMousem.Series.Add(series24);
            this.chMousem.Size = new System.Drawing.Size(387, 188);
            this.chMousem.TabIndex = 7;
            this.chMousem.Text = "chart1";
            // 
            // frmCuatro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(922, 593);
            this.Controls.Add(this.lblPracticas);
            this.Controls.Add(this.lblRegistros);
            this.Controls.Add(this.chGrafica2);
            this.Controls.Add(this.lblSalir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chGrafica);
            this.Controls.Add(this.chMousem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCuatro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCuatro";
            this.Load += new System.EventHandler(this.frmCuatro_Load);
            this.Click += new System.EventHandler(this.frmCuatro_Click);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmCuatro_KeyPress);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmCuatro_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.chGrafica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chGrafica2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chMousem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chGrafica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSalir;
        private System.Windows.Forms.DataVisualization.Charting.Chart chGrafica2;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.Label lblPracticas;
        private System.Windows.Forms.Timer tmrMouse;
        private System.Windows.Forms.DataVisualization.Charting.Chart chMousem;
    }
}