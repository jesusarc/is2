﻿namespace IS2
{
    partial class RegistroPrestamo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.lblPrestamista = new System.Windows.Forms.Label();
            this.cbPrestamistas = new System.Windows.Forms.ComboBox();
            this.cbIva = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDedudor = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(422, 205);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 0;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(341, 205);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(12, 84);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(266, 20);
            this.txtCantidad.TabIndex = 2;
            this.txtCantidad.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Location = new System.Drawing.Point(12, 65);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(49, 13);
            this.lblCantidad.TabIndex = 3;
            this.lblCantidad.Text = "Cantidad";
            // 
            // lblPrestamista
            // 
            this.lblPrestamista.AutoSize = true;
            this.lblPrestamista.Location = new System.Drawing.Point(9, 14);
            this.lblPrestamista.Name = "lblPrestamista";
            this.lblPrestamista.Size = new System.Drawing.Size(61, 13);
            this.lblPrestamista.TabIndex = 3;
            this.lblPrestamista.Text = "Prestamista";
            // 
            // cbPrestamistas
            // 
            this.cbPrestamistas.FormattingEnabled = true;
            this.cbPrestamistas.Location = new System.Drawing.Point(12, 31);
            this.cbPrestamistas.Name = "cbPrestamistas";
            this.cbPrestamistas.Size = new System.Drawing.Size(121, 21);
            this.cbPrestamistas.TabIndex = 4;
            // 
            // cbIva
            // 
            this.cbIva.FormattingEnabled = true;
            this.cbIva.Location = new System.Drawing.Point(15, 136);
            this.cbIva.Name = "cbIva";
            this.cbIva.Size = new System.Drawing.Size(121, 21);
            this.cbIva.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "IVA";
            // 
            // cbDedudor
            // 
            this.cbDedudor.FormattingEnabled = true;
            this.cbDedudor.Location = new System.Drawing.Point(15, 194);
            this.cbDedudor.Name = "cbDedudor";
            this.cbDedudor.Size = new System.Drawing.Size(121, 21);
            this.cbDedudor.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Deudor";
            // 
            // RegistroPrestamo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 240);
            this.Controls.Add(this.cbDedudor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbIva);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPrestamistas);
            this.Controls.Add(this.lblPrestamista);
            this.Controls.Add(this.lblCantidad);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Name = "RegistroPrestamo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RegistroPrestamo";
            this.Load += new System.EventHandler(this.RegistroPrestamo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.Label lblPrestamista;
        private System.Windows.Forms.ComboBox cbPrestamistas;
        private System.Windows.Forms.ComboBox cbIva;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDedudor;
        private System.Windows.Forms.Label label2;
    }
}