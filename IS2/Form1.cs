﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Método en el que se sale de la aplicación.
        private void lblSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //Se llama a la forma uno
        private void label6_Click(object sender, EventArgs e)
        {
            datos.practica1++;
            new frmUno().Show();
            this.Hide();
        }
        //Se llama a la forma dos
        private void label7_Click(object sender, EventArgs e)
        {
            datos.practica2++;
            new frmDos().Show();
            this.Hide();
        }
        //Método que manda llamar la práctica tres
        private void label8_Click(object sender, EventArgs e)
        {
            datos.practica3++;
            new frmTres().Show();
            this.Hide();
        }
        //Método que manda llamar la práctica cuatro
        private void label9_Click(object sender, EventArgs e)
        {
            datos.practica4++;
            new frmCuatro().Show();
            this.Hide();
        }
        //Métofdo que manda llamar la práctica cinco
        private void label10_Click(object sender, EventArgs e)
        {
            new frmCinco().Show();
            this.Hide();
        }
        //Método que manda llamas la práctica seis
        private void label11_Click(object sender, EventArgs e)
        {
            new frmSeis().Show();
            this.Hide();
        }
        //Método que manda llamar la práctica siete
        private void label12_Click(object sender, EventArgs e)
        {
            new frmSiete().Show();
            this.Hide();
        }
        //Método que manda llamar la práctica ocho
        private void label13_Click(object sender, EventArgs e)
        {
            new frmOcho().Show();
            this.Hide();
        }
        //Método que manda llamar la práctica nueve
        private void label14_Click(object sender, EventArgs e)
        {
            new frmBackup().Show();
            this.Hide();
        }
    }
}
