﻿using System;
using System.Windows.Forms;
using System.IO;

namespace IS2
{
    public partial class frmBackup : Form
    {
        public frmBackup()
        {
            InitializeComponent();
        }
        //Se cierra la práctica y se regresa al menú principal
        private void lblSalir_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Close();
        }
        //Método que se ejecuta al dar click en el icono de respaldo
        private void pbRespaldar_Click(object sender, EventArgs e)
        {
            //Validar que se selecciona al menos una casilla
            if (!chkExcel.Checked && !chkWord.Checked && !chkPower.Checked && !chkJpg.Checked && !chkPdf.Checked && !chkPng.Checked) MessageBox.Show("Debes seleccionar al menos un tipo de archivo");
            //Se declaran las variables para comprobar los checkbox
            bool excel = false, word = false, power = false, pdf = false, jpg = false, png = false;
            //Se validan los checkbox
            if (chkExcel.Checked) excel = true; 
            if (chkWord.Checked) word = true; 
            if (chkPower.Checked) power = true;
            if (chkPdf.Checked) pdf = true;  
            if (chkJpg.Checked)  jpg = true;
            if (chkPng.Checked) png = true;
            //Se ejecutan las copias
            if (excel) { copiaArchivos(".xls");  copiaArchivos(".xlsx"); }
            if (word) { copiaArchivos(".doc"); copiaArchivos(".docx"); }
            if (power) { copiaArchivos(".ppt"); copiaArchivos(".pptx"); }
            if (pdf) copiaArchivos(".pdf");
            if (jpg) { copiaArchivos(".jpg"); copiaArchivos(".jpeg"); }
            if (png) copiaArchivos(".png");
            //Mensaje de termino de respaldo
            pbRespaldo.Value = pbRespaldo.Maximum;
            MessageBox.Show("Respaldo realizado con éxito");

        }
        //Método con el que se leen y copian los archivos
        private void copiaArchivos(string extension)
        {
            if (pbRespaldo.Value == pbRespaldo.Maximum) pbRespaldo.Value = pbRespaldo.Minimum;
            //Se declara la ruta de destino
            string carpetaDestino = @"" + txtRuta.Text + "";
            //Se declara la ruta de origen
            DirectoryInfo carpetaOrigen = new DirectoryInfo(@"" + txtRutaOrigen.Text + "");
            //Se elige la extension a respaldar
            FileInfo[] archivos = carpetaOrigen.GetFiles("*"+extension+"");
            //Se recorren los archivos y se van copiando
            foreach (FileInfo files in archivos)
            {
                string fullPathDestination = Path.Combine(carpetaDestino, files.Name);
                File.Copy(files.FullName, fullPathDestination, true); //Si ya existe un archivo se sobreescribe
                pbRespaldo.Increment(1); //Se crea el efecto de carga de archivos
            } 
            
        }
        //Selección de la ruta donde van a guardarse los respaldos
        private void btnExaminar_Click(object sender, EventArgs e)
        {
            fbdGuardar.ShowDialog();
            txtRuta.Text = fbdGuardar.SelectedPath.ToString();
        }
        //Selección de la ruta de origen de respaldo
        private void btnSeleccionarOrigen_Click(object sender, EventArgs e)
        {
            fbdOrigen.ShowDialog();
            txtRutaOrigen.Text = fbdOrigen.SelectedPath.ToString();
        }
    }
}
