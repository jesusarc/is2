﻿using System;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmUno : Form
    {
        public frmUno()
        {
            InitializeComponent();
        }
        //Método que funciona cuando se da click en la etiqueta para salir
        private void lblSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 menu = new Form1();
            menu.Show();
        }
        //Método que funciona al cargar la forma.
        private void frmUno_Load(object sender, EventArgs e)
        {
            //Orientación de la barra
            tbPrincipal.Orientation = Orientation.Vertical;
            //Se genera un número aleatorio
            Random r = new Random();
            int numero = r.Next(1, 12);
            //Se asigna el número aleatorio al nombre del archivo para después asignarlo al picturebox
            pbCentral.Image = System.Drawing.Image.FromFile(Application.StartupPath + "..\\practicauno\\" + numero.ToString() + ".png");
        }
        //Arrancar el timer
        private void pbStart_Click(object sender, EventArgs e)
        {
            tmDos.Start();
        }

        private void tmDos_Tick(object sender, EventArgs e)
        {
            //Se generra un número aleatorio
            Random r = new Random();
            int numero = r.Next(1, 12);
            //Se asigna el número aleatorio al nombre del archivo para después asignarlo al picturebox
            pbCentral.Image = System.Drawing.Image.FromFile(Application.StartupPath + "..\\practicauno\\" + numero.ToString() + ".png");
        }
        //Método que detiene el timer
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            tmDos.Stop();
        }
        //Método en el que se cambia el valor del tiempo en base al scroll del trackbar
        private void tbPrincipal_Scroll(object sender, EventArgs e)
        {
            // Se cambia el tiempo de la iteración del timer en base al valor del trackbar
            tmDos.Interval = tbPrincipal.Value * -100 + 1300;
        }
    }
}
