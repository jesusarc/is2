﻿namespace IS2
{
    partial class frmSeis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSalir = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblPrestamistas = new System.Windows.Forms.Label();
            this.dgvPrestamistas = new System.Windows.Forms.DataGridView();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.febrero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marzo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abril = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mayo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.junio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.julio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.agosto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.septiembre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.octubre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noviembre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diciembre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDeudores = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDeudores = new System.Windows.Forms.Label();
            this.lblPrestamos = new System.Windows.Forms.Label();
            this.dgvPrestamos = new System.Windows.Forms.DataGridView();
            this.deudor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prestamo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.interes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prestamista = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deuda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtFecha = new System.Windows.Forms.DateTimePicker();
            this.cbDedudor = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbIva = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPrestamistas = new System.Windows.Forms.ComboBox();
            this.lblPrestamista = new System.Windows.Forms.Label();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrestamistas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeudores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrestamos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSalir
            // 
            this.lblSalir.AutoSize = true;
            this.lblSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSalir.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalir.ForeColor = System.Drawing.Color.Teal;
            this.lblSalir.Location = new System.Drawing.Point(15, 650);
            this.lblSalir.Name = "lblSalir";
            this.lblSalir.Size = new System.Drawing.Size(54, 25);
            this.lblSalir.TabIndex = 5;
            this.lblSalir.Text = "Salir";
            this.lblSalir.Click += new System.EventHandler(this.lblSalir_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.Teal;
            this.lblTitulo.Location = new System.Drawing.Point(370, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(182, 25);
            this.lblTitulo.TabIndex = 6;
            this.lblTitulo.Text = "Tabla de Intereses";
            // 
            // lblPrestamistas
            // 
            this.lblPrestamistas.AutoSize = true;
            this.lblPrestamistas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPrestamistas.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrestamistas.ForeColor = System.Drawing.Color.Teal;
            this.lblPrestamistas.Location = new System.Drawing.Point(13, 38);
            this.lblPrestamistas.Name = "lblPrestamistas";
            this.lblPrestamistas.Size = new System.Drawing.Size(231, 17);
            this.lblPrestamistas.TabIndex = 7;
            this.lblPrestamistas.Text = "Prestamistas (Intereses a recibir)";
            // 
            // dgvPrestamistas
            // 
            this.dgvPrestamistas.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPrestamistas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrestamistas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombre,
            this.enero,
            this.febrero,
            this.marzo,
            this.abril,
            this.mayo,
            this.junio,
            this.julio,
            this.agosto,
            this.septiembre,
            this.octubre,
            this.noviembre,
            this.diciembre});
            this.dgvPrestamistas.Location = new System.Drawing.Point(16, 58);
            this.dgvPrestamistas.Name = "dgvPrestamistas";
            this.dgvPrestamistas.RowHeadersVisible = false;
            this.dgvPrestamistas.Size = new System.Drawing.Size(893, 150);
            this.dgvPrestamistas.TabIndex = 8;
            // 
            // nombre
            // 
            this.nombre.Frozen = true;
            this.nombre.HeaderText = "Nombre";
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            // 
            // enero
            // 
            this.enero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.enero.Frozen = true;
            this.enero.HeaderText = "Enero";
            this.enero.Name = "enero";
            this.enero.ReadOnly = true;
            this.enero.Width = 60;
            // 
            // febrero
            // 
            this.febrero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.febrero.Frozen = true;
            this.febrero.HeaderText = "Febrero";
            this.febrero.Name = "febrero";
            this.febrero.ReadOnly = true;
            this.febrero.Width = 68;
            // 
            // marzo
            // 
            this.marzo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.marzo.Frozen = true;
            this.marzo.HeaderText = "Marzo";
            this.marzo.Name = "marzo";
            this.marzo.ReadOnly = true;
            this.marzo.Width = 61;
            // 
            // abril
            // 
            this.abril.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.abril.Frozen = true;
            this.abril.HeaderText = "Abril";
            this.abril.Name = "abril";
            this.abril.ReadOnly = true;
            this.abril.Width = 52;
            // 
            // mayo
            // 
            this.mayo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.mayo.Frozen = true;
            this.mayo.HeaderText = "Mayo";
            this.mayo.Name = "mayo";
            this.mayo.ReadOnly = true;
            this.mayo.Width = 58;
            // 
            // junio
            // 
            this.junio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.junio.Frozen = true;
            this.junio.HeaderText = "Junio";
            this.junio.Name = "junio";
            this.junio.ReadOnly = true;
            this.junio.Width = 57;
            // 
            // julio
            // 
            this.julio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.julio.Frozen = true;
            this.julio.HeaderText = "Julio";
            this.julio.Name = "julio";
            this.julio.ReadOnly = true;
            this.julio.Width = 53;
            // 
            // agosto
            // 
            this.agosto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.agosto.Frozen = true;
            this.agosto.HeaderText = "Agosto";
            this.agosto.Name = "agosto";
            this.agosto.ReadOnly = true;
            this.agosto.Width = 65;
            // 
            // septiembre
            // 
            this.septiembre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.septiembre.Frozen = true;
            this.septiembre.HeaderText = "Septiembre";
            this.septiembre.Name = "septiembre";
            this.septiembre.ReadOnly = true;
            this.septiembre.Width = 85;
            // 
            // octubre
            // 
            this.octubre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.octubre.Frozen = true;
            this.octubre.HeaderText = "Octubre";
            this.octubre.Name = "octubre";
            this.octubre.ReadOnly = true;
            this.octubre.Width = 70;
            // 
            // noviembre
            // 
            this.noviembre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.noviembre.Frozen = true;
            this.noviembre.HeaderText = "Noviembre";
            this.noviembre.Name = "noviembre";
            this.noviembre.ReadOnly = true;
            this.noviembre.Width = 83;
            // 
            // diciembre
            // 
            this.diciembre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.diciembre.Frozen = true;
            this.diciembre.HeaderText = "Diciembre";
            this.diciembre.Name = "diciembre";
            this.diciembre.ReadOnly = true;
            this.diciembre.Width = 79;
            // 
            // dgvDeudores
            // 
            this.dgvDeudores.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvDeudores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeudores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.dgvDeudores.Location = new System.Drawing.Point(17, 243);
            this.dgvDeudores.Name = "dgvDeudores";
            this.dgvDeudores.RowHeadersVisible = false;
            this.dgvDeudores.Size = new System.Drawing.Size(893, 150);
            this.dgvDeudores.TabIndex = 10;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Enero";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 60;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "Febrero";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 68;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "Marzo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 61;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "Abril";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 52;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn6.Frozen = true;
            this.dataGridViewTextBoxColumn6.HeaderText = "Mayo";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 58;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn7.Frozen = true;
            this.dataGridViewTextBoxColumn7.HeaderText = "Junio";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 57;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn8.Frozen = true;
            this.dataGridViewTextBoxColumn8.HeaderText = "Julio";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 53;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn9.Frozen = true;
            this.dataGridViewTextBoxColumn9.HeaderText = "Agosto";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 65;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn10.Frozen = true;
            this.dataGridViewTextBoxColumn10.HeaderText = "Septiembre";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 85;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn11.Frozen = true;
            this.dataGridViewTextBoxColumn11.HeaderText = "Octubre";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 70;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn12.Frozen = true;
            this.dataGridViewTextBoxColumn12.HeaderText = "Noviembre";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 83;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn13.Frozen = true;
            this.dataGridViewTextBoxColumn13.HeaderText = "Diciembre";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 79;
            // 
            // lblDeudores
            // 
            this.lblDeudores.AutoSize = true;
            this.lblDeudores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDeudores.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeudores.ForeColor = System.Drawing.Color.Teal;
            this.lblDeudores.Location = new System.Drawing.Point(14, 223);
            this.lblDeudores.Name = "lblDeudores";
            this.lblDeudores.Size = new System.Drawing.Size(207, 17);
            this.lblDeudores.TabIndex = 9;
            this.lblDeudores.Text = "Deudores (Intereses a pagar)";
            // 
            // lblPrestamos
            // 
            this.lblPrestamos.AutoSize = true;
            this.lblPrestamos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPrestamos.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrestamos.ForeColor = System.Drawing.Color.Teal;
            this.lblPrestamos.Location = new System.Drawing.Point(441, 411);
            this.lblPrestamos.Name = "lblPrestamos";
            this.lblPrestamos.Size = new System.Drawing.Size(133, 17);
            this.lblPrestamos.TabIndex = 11;
            this.lblPrestamos.Text = "Relación de Pagos";
            // 
            // dgvPrestamos
            // 
            this.dgvPrestamos.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPrestamos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrestamos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.deudor,
            this.prestamo,
            this.fecha,
            this.interes,
            this.prestamista,
            this.abono,
            this.deuda});
            this.dgvPrestamos.Location = new System.Drawing.Point(169, 431);
            this.dgvPrestamos.Name = "dgvPrestamos";
            this.dgvPrestamos.RowHeadersVisible = false;
            this.dgvPrestamos.Size = new System.Drawing.Size(740, 140);
            this.dgvPrestamos.TabIndex = 12;
            // 
            // deudor
            // 
            this.deudor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.deudor.Frozen = true;
            this.deudor.HeaderText = "Deudor";
            this.deudor.Name = "deudor";
            this.deudor.ReadOnly = true;
            this.deudor.Width = 67;
            // 
            // prestamo
            // 
            this.prestamo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.prestamo.Frozen = true;
            this.prestamo.HeaderText = "Prestamo";
            this.prestamo.Name = "prestamo";
            this.prestamo.ReadOnly = true;
            this.prestamo.Width = 76;
            // 
            // fecha
            // 
            this.fecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.fecha.Frozen = true;
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            this.fecha.Width = 62;
            // 
            // interes
            // 
            this.interes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.interes.Frozen = true;
            this.interes.HeaderText = "% de Interes";
            this.interes.Name = "interes";
            this.interes.ReadOnly = true;
            this.interes.Width = 90;
            // 
            // prestamista
            // 
            this.prestamista.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.prestamista.Frozen = true;
            this.prestamista.HeaderText = "Prestamista";
            this.prestamista.Name = "prestamista";
            this.prestamista.ReadOnly = true;
            this.prestamista.Width = 86;
            // 
            // abono
            // 
            this.abono.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.abono.Frozen = true;
            this.abono.HeaderText = "Abonos";
            this.abono.Name = "abono";
            this.abono.ReadOnly = true;
            this.abono.Width = 68;
            // 
            // deuda
            // 
            this.deuda.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.deuda.Frozen = true;
            this.deuda.HeaderText = "Deuda";
            this.deuda.Name = "deuda";
            this.deuda.ReadOnly = true;
            this.deuda.Width = 64;
            // 
            // dtFecha
            // 
            this.dtFecha.Location = new System.Drawing.Point(709, 14);
            this.dtFecha.Name = "dtFecha";
            this.dtFecha.Size = new System.Drawing.Size(200, 20);
            this.dtFecha.TabIndex = 14;
            // 
            // cbDedudor
            // 
            this.cbDedudor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDedudor.FormattingEnabled = true;
            this.cbDedudor.Location = new System.Drawing.Point(20, 594);
            this.cbDedudor.Name = "cbDedudor";
            this.cbDedudor.Size = new System.Drawing.Size(121, 21);
            this.cbDedudor.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 577);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Deudor";
            // 
            // cbIva
            // 
            this.cbIva.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIva.FormattingEnabled = true;
            this.cbIva.Location = new System.Drawing.Point(20, 536);
            this.cbIva.Name = "cbIva";
            this.cbIva.Size = new System.Drawing.Size(121, 21);
            this.cbIva.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 520);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "IVA";
            // 
            // cbPrestamistas
            // 
            this.cbPrestamistas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPrestamistas.FormattingEnabled = true;
            this.cbPrestamistas.Location = new System.Drawing.Point(17, 431);
            this.cbPrestamistas.Name = "cbPrestamistas";
            this.cbPrestamistas.Size = new System.Drawing.Size(121, 21);
            this.cbPrestamistas.TabIndex = 18;
            // 
            // lblPrestamista
            // 
            this.lblPrestamista.AutoSize = true;
            this.lblPrestamista.Location = new System.Drawing.Point(14, 414);
            this.lblPrestamista.Name = "lblPrestamista";
            this.lblPrestamista.Size = new System.Drawing.Size(61, 13);
            this.lblPrestamista.TabIndex = 16;
            this.lblPrestamista.Text = "Prestamista";
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Location = new System.Drawing.Point(17, 465);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(49, 13);
            this.lblCantidad.TabIndex = 17;
            this.lblCantidad.Text = "Cantidad";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(17, 484);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(124, 20);
            this.txtCantidad.TabIndex = 15;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(169, 594);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 23;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::IS2.Properties.Resources.Adobe_Acrobat_Reader_8;
            this.pictureBox1.Location = new System.Drawing.Point(837, 605);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(72, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // frmSeis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 681);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.cbDedudor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbIva);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPrestamistas);
            this.Controls.Add(this.lblPrestamista);
            this.Controls.Add(this.lblCantidad);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.dtFecha);
            this.Controls.Add(this.dgvPrestamos);
            this.Controls.Add(this.lblPrestamos);
            this.Controls.Add(this.dgvDeudores);
            this.Controls.Add(this.lblDeudores);
            this.Controls.Add(this.dgvPrestamistas);
            this.Controls.Add(this.lblPrestamistas);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblSalir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSeis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSeis";
            this.Load += new System.EventHandler(this.frmSeis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrestamistas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeudores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrestamos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSalir;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblPrestamistas;
        private System.Windows.Forms.DataGridView dgvPrestamistas;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn enero;
        private System.Windows.Forms.DataGridViewTextBoxColumn febrero;
        private System.Windows.Forms.DataGridViewTextBoxColumn marzo;
        private System.Windows.Forms.DataGridViewTextBoxColumn abril;
        private System.Windows.Forms.DataGridViewTextBoxColumn mayo;
        private System.Windows.Forms.DataGridViewTextBoxColumn junio;
        private System.Windows.Forms.DataGridViewTextBoxColumn julio;
        private System.Windows.Forms.DataGridViewTextBoxColumn agosto;
        private System.Windows.Forms.DataGridViewTextBoxColumn septiembre;
        private System.Windows.Forms.DataGridViewTextBoxColumn octubre;
        private System.Windows.Forms.DataGridViewTextBoxColumn noviembre;
        private System.Windows.Forms.DataGridViewTextBoxColumn diciembre;
        private System.Windows.Forms.DataGridView dgvDeudores;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.Label lblDeudores;
        private System.Windows.Forms.Label lblPrestamos;
        private System.Windows.Forms.DataGridView dgvPrestamos;
        private System.Windows.Forms.DataGridViewTextBoxColumn deudor;
        private System.Windows.Forms.DataGridViewTextBoxColumn prestamo;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn interes;
        private System.Windows.Forms.DataGridViewTextBoxColumn prestamista;
        private System.Windows.Forms.DataGridViewTextBoxColumn abono;
        private System.Windows.Forms.DataGridViewTextBoxColumn deuda;
        private System.Windows.Forms.DateTimePicker dtFecha;
        private System.Windows.Forms.ComboBox cbDedudor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbIva;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPrestamistas;
        private System.Windows.Forms.Label lblPrestamista;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}