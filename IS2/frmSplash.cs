﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();
        }
        //Se establece el fondo transparente los label en base al fondo de la forma.
        private void frmSplash_Load(object sender, EventArgs e)
        {
            lblBienvenida.Parent = pbFondo;
            lblCargando.Parent = pbFondo;
            tmrCarga.Start();
        }
        //Método en el que funciona la barra de carga
        private void tmrCarga_Tick(object sender, EventArgs e)
        {
            //Se incrementa el valor de la barra de carga en base al timer
            pbrCarga.Increment(1);
            //Se cambia el fondo del texto donde se indica la carga
            lblPorcent.Parent = pbFondo;
            lblPorcent.Text = pbrCarga.Value.ToString() + "% Completado";
            if(pbrCarga.Value == pbrCarga.Maximum)
            {
                Form1 menu = new Form1();
                menu.Show();
                this.Hide();
                tmrCarga.Stop();            }
        }
    }
}
