﻿namespace IS2
{
    partial class frmJuego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlScore = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblScore = new System.Windows.Forms.Label();
            this.pbShip = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbPanel = new System.Windows.Forms.PictureBox();
            this.pbWing = new System.Windows.Forms.PictureBox();
            this.pbShip2 = new System.Windows.Forms.PictureBox();
            this.pbShip3 = new System.Windows.Forms.PictureBox();
            this.tmrJuego = new System.Windows.Forms.Timer(this.components);
            this.lblPuntuacion = new System.Windows.Forms.Label();
            this.pbNivel = new System.Windows.Forms.Label();
            this.pnlScore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbShip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbShip2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbShip3)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlScore
            // 
            this.pnlScore.BackColor = System.Drawing.Color.Black;
            this.pnlScore.Controls.Add(this.pbNivel);
            this.pnlScore.Controls.Add(this.lblPuntuacion);
            this.pnlScore.Controls.Add(this.label1);
            this.pnlScore.Controls.Add(this.lblScore);
            this.pnlScore.Location = new System.Drawing.Point(-3, 511);
            this.pnlScore.Name = "pnlScore";
            this.pnlScore.Size = new System.Drawing.Size(914, 46);
            this.pnlScore.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Goldenrod;
            this.label1.Location = new System.Drawing.Point(15, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Level :";
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScore.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblScore.Location = new System.Drawing.Point(177, 13);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(71, 20);
            this.lblScore.TabIndex = 0;
            this.lblScore.Text = "Score : ";
            // 
            // pbShip
            // 
            this.pbShip.Image = global::IS2.Properties.Resources.republic_assault_ship;
            this.pbShip.Location = new System.Drawing.Point(324, -3);
            this.pbShip.Name = "pbShip";
            this.pbShip.Size = new System.Drawing.Size(69, 71);
            this.pbShip.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbShip.TabIndex = 18;
            this.pbShip.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox15.Location = new System.Drawing.Point(606, 405);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(134, 166);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 16;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox14.Location = new System.Drawing.Point(739, 405);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(172, 166);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 15;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox13.Location = new System.Drawing.Point(606, 322);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(134, 166);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 14;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox12.Location = new System.Drawing.Point(739, 322);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(172, 166);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 13;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox11.Location = new System.Drawing.Point(606, 158);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(134, 166);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 12;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox10.Location = new System.Drawing.Point(739, 158);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(172, 166);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 11;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox9.Location = new System.Drawing.Point(606, -3);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(134, 166);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 10;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox8.Location = new System.Drawing.Point(739, -3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(172, 166);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 9;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox7.Location = new System.Drawing.Point(166, 445);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(129, 166);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 8;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox6.Location = new System.Drawing.Point(-4, 444);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(172, 166);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 7;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox5.Location = new System.Drawing.Point(166, 322);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(129, 166);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 6;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox4.Location = new System.Drawing.Point(-4, 322);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(172, 166);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 5;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox3.Location = new System.Drawing.Point(166, 158);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(129, 166);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox2.Location = new System.Drawing.Point(-4, 158);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(172, 166);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::IS2.Properties.Resources.DS231;
            this.pictureBox1.Location = new System.Drawing.Point(166, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(129, 166);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pbPanel
            // 
            this.pbPanel.Image = global::IS2.Properties.Resources.DS231;
            this.pbPanel.Location = new System.Drawing.Point(-4, -3);
            this.pbPanel.Name = "pbPanel";
            this.pbPanel.Size = new System.Drawing.Size(172, 166);
            this.pbPanel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPanel.TabIndex = 1;
            this.pbPanel.TabStop = false;
            // 
            // pbWing
            // 
            this.pbWing.Image = global::IS2.Properties.Resources.X_Wing_Game_Fighter;
            this.pbWing.Location = new System.Drawing.Point(411, 418);
            this.pbWing.Name = "pbWing";
            this.pbWing.Size = new System.Drawing.Size(84, 87);
            this.pbWing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbWing.TabIndex = 0;
            this.pbWing.TabStop = false;
            // 
            // pbShip2
            // 
            this.pbShip2.Image = global::IS2.Properties.Resources.republic_assault_ship;
            this.pbShip2.Location = new System.Drawing.Point(517, -3);
            this.pbShip2.Name = "pbShip2";
            this.pbShip2.Size = new System.Drawing.Size(69, 71);
            this.pbShip2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbShip2.TabIndex = 19;
            this.pbShip2.TabStop = false;
            // 
            // pbShip3
            // 
            this.pbShip3.Image = global::IS2.Properties.Resources.republic_assault_ship;
            this.pbShip3.Location = new System.Drawing.Point(426, -3);
            this.pbShip3.Name = "pbShip3";
            this.pbShip3.Size = new System.Drawing.Size(69, 71);
            this.pbShip3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbShip3.TabIndex = 20;
            this.pbShip3.TabStop = false;
            this.pbShip3.Visible = false;
            // 
            // tmrJuego
            // 
            this.tmrJuego.Enabled = true;
            this.tmrJuego.Interval = 50;
            this.tmrJuego.Tick += new System.EventHandler(this.tmrJuego_Tick);
            // 
            // lblPuntuacion
            // 
            this.lblPuntuacion.AutoSize = true;
            this.lblPuntuacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPuntuacion.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblPuntuacion.Location = new System.Drawing.Point(254, 13);
            this.lblPuntuacion.Name = "lblPuntuacion";
            this.lblPuntuacion.Size = new System.Drawing.Size(19, 20);
            this.lblPuntuacion.TabIndex = 2;
            this.lblPuntuacion.Text = "0";
            // 
            // pbNivel
            // 
            this.pbNivel.AutoSize = true;
            this.pbNivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbNivel.ForeColor = System.Drawing.Color.Goldenrod;
            this.pbNivel.Location = new System.Drawing.Point(82, 13);
            this.pbNivel.Name = "pbNivel";
            this.pbNivel.Size = new System.Drawing.Size(19, 20);
            this.pbNivel.TabIndex = 3;
            this.pbNivel.Text = "1";
            // 
            // frmJuego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(906, 554);
            this.Controls.Add(this.pbWing);
            this.Controls.Add(this.pnlScore);
            this.Controls.Add(this.pbShip3);
            this.Controls.Add(this.pbShip2);
            this.Controls.Add(this.pbShip);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmJuego";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmJuego";
            this.Load += new System.EventHandler(this.frmJuego_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmJuego_KeyPress);
            this.pnlScore.ResumeLayout(false);
            this.pnlScore.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbShip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbShip2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbShip3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbWing;
        private System.Windows.Forms.PictureBox pbPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Panel pnlScore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.PictureBox pbShip;
        private System.Windows.Forms.PictureBox pbShip2;
        private System.Windows.Forms.PictureBox pbShip3;
        private System.Windows.Forms.Timer tmrJuego;
        private System.Windows.Forms.Label lblPuntuacion;
        private System.Windows.Forms.Label pbNivel;
    }
}