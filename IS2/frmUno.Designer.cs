﻿namespace IS2
{
    partial class frmUno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblSalir = new System.Windows.Forms.Label();
            this.tbPrincipal = new System.Windows.Forms.TrackBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbStart = new System.Windows.Forms.PictureBox();
            this.pbCentral = new System.Windows.Forms.PictureBox();
            this.tmDos = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tbPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCentral)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSalir
            // 
            this.lblSalir.AutoSize = true;
            this.lblSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSalir.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalir.ForeColor = System.Drawing.Color.Teal;
            this.lblSalir.Location = new System.Drawing.Point(12, 559);
            this.lblSalir.Name = "lblSalir";
            this.lblSalir.Size = new System.Drawing.Size(94, 25);
            this.lblSalir.TabIndex = 1;
            this.lblSalir.Text = "Regresar";
            this.lblSalir.Click += new System.EventHandler(this.lblSalir_Click);
            // 
            // tbPrincipal
            // 
            this.tbPrincipal.Location = new System.Drawing.Point(667, 175);
            this.tbPrincipal.Name = "tbPrincipal";
            this.tbPrincipal.Size = new System.Drawing.Size(243, 45);
            this.tbPrincipal.TabIndex = 3;
            this.tbPrincipal.Scroll += new System.EventHandler(this.tbPrincipal_Scroll);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::IS2.Properties.Resources.ic_pause_white_36dp_1x;
            this.pictureBox1.Location = new System.Drawing.Point(468, 366);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(36, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pbStart
            // 
            this.pbStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbStart.Image = global::IS2.Properties.Resources.ic_play_arrow_white_36dp_1x;
            this.pbStart.Location = new System.Drawing.Point(419, 366);
            this.pbStart.Name = "pbStart";
            this.pbStart.Size = new System.Drawing.Size(36, 36);
            this.pbStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbStart.TabIndex = 4;
            this.pbStart.TabStop = false;
            this.pbStart.Click += new System.EventHandler(this.pbStart_Click);
            // 
            // pbCentral
            // 
            this.pbCentral.Image = global::IS2.Properties.Resources._2;
            this.pbCentral.Location = new System.Drawing.Point(397, 232);
            this.pbCentral.Name = "pbCentral";
            this.pbCentral.Size = new System.Drawing.Size(128, 128);
            this.pbCentral.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbCentral.TabIndex = 2;
            this.pbCentral.TabStop = false;
            // 
            // tmDos
            // 
            this.tmDos.Enabled = true;
            this.tmDos.Interval = 800;
            this.tmDos.Tick += new System.EventHandler(this.tmDos_Tick);
            // 
            // frmUno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(922, 593);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbStart);
            this.Controls.Add(this.tbPrincipal);
            this.Controls.Add(this.pbCentral);
            this.Controls.Add(this.lblSalir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmUno";
            this.Load += new System.EventHandler(this.frmUno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCentral)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSalir;
        private System.Windows.Forms.PictureBox pbCentral;
        private System.Windows.Forms.TrackBar tbPrincipal;
        private System.Windows.Forms.PictureBox pbStart;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer tmDos;
    }
}