﻿namespace IS2
{
    class datos
    {
        //Variables globales del sistema
        //En estas variables se almacenan los usuario que tienen acceso.
        public static string user1 = "admin";
        public static string pass1 = "admin";
        public static string user2 = "chubuntu";
        public static string pass2 = "Ubuntuarc1";
        public static string user3 = "test";
        public static string pass3 = "123456";
        public static int practica1, practica2, practica3, practica4;

        //Declaración de variables.
        public static double[] interes_prestamista_chuy = new double[12];
        public static double[] interes_prestamista_karen = new double[12];
        public static double[] interes_prestamista_rodrigo = new double[12];
        public static double[] interes_prestamista_lorena = new double[12];
        public static double[] interes_prestamista_pepe = new double[12];
        public static double[] interes_deudor_isis = new double[12];
        public static double[] interes_deudor_raquel = new double[12];
        public static double[] interes_deudor_ivan = new double[12];
        public static double[] interes_deudor_melissa = new double[12];
        public static double[] interes_deudor_omar = new double[12];
    }
}
