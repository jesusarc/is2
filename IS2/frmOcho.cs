﻿using System;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmOcho : Form
    {
        public frmOcho()
        {
            InitializeComponent();
        }
        //Se cierra la práctica y se regresa al menú prinicpal.
        private void lblSalir_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Close();
        }
        //Boton para mostrar el escritorio a distancia
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //Se manda a llamar a la forma que muestra el escritorio
            new frmRemoto(int.Parse(txtPuerto.Text)).Show(); //Se envia como parametro el puerto escrito en el textbox de la forma actual
        }
    }
}
