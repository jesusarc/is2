﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IS2
{
    public partial class frmJuego : Form
    {
        //Variable de puntiación
        public static int score = 0;
        //Variable de nivel
        public static int nivel = 1;
        //Variable de velocidad de las naves
        public static int velocidad = 0;
        public frmJuego()
        {
            InitializeComponent();
        }

        private void frmJuego_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
                Form1 menu = new Form1();
                menu.Show();
            }
            if (e.KeyChar.ToString()=="d")
            {
                pbWing.Location = new Point(pbWing.Location.X + 10, pbWing.Location.Y);
            }
            if (e.KeyChar.ToString() == "a")
            {
                pbWing.Location = new Point(pbWing.Location.X - 10, pbWing.Location.Y);
            }

        }
        //Método que funciona dentro del timer del juego
        private void tmrJuego_Tick(object sender, EventArgs e)
        {
            #region //Evaluaciones que detectan cuando las anves llegan al final de la forma.
            if (pbShip.Top >= this.Height)
            {
                //Cuando llega al punto final la nave regresa al punto 0 en Y.
                pbShip.Location = new Point(pbShip.Location.X, 0);
                //Condicion en la que, si la nave es invisible aparece y viceversa.
                if (pbShip.Visible == true) pbShip.Visible = false; else pbShip.Visible = true;
                //Condición en la que la que si la nave es visible al momento de llegar al final
                //Se suman 100 puntos y se muestran en pantalla.
                if (pbShip.Visible == false)
                {
                    score += 100;
                    lblPuntuacion.Text = score.ToString();
                    pbNivel.Text = nivel.ToString();
                }
            }
            if (pbShip2.Top >= this.Height)
            {
                //Cuando llega al punto final la nave regresa al punto 0 en Y.
                pbShip2.Location = new Point(pbShip2.Location.X, 0);
                //Condicion en la que, si la nave es invisible aparece y viceversa.
                if (pbShip2.Visible == true) pbShip2.Visible = false; else pbShip2.Visible = true;
                //Condición en la que la que si la nave es visible al momento de llegar al final
                //Se suman 100 puntos y se muestran en pantalla.
                if (pbShip2.Visible == false)
                {
                    score += 100;
                    lblPuntuacion.Text = score.ToString();
                    pbNivel.Text = nivel.ToString();
                }
            }
            if (pbShip3.Top >= this.Height)
            {
                //Cuando llega al punto final la nave regresa al punto 0 en Y.
                pbShip3.Location = new Point(pbShip3.Location.X, 0);
                //Condicion en la que, si la nave es invisible aparece y viceversa.
                if (pbShip3.Visible == true) pbShip3.Visible = false; else pbShip3.Visible = true;
                //Condición en la que la que si la nave es visible al momento de llegar al final
                //Se suman 100 puntos y se muestran en pantalla.
                if (pbShip3.Visible == false)
                {
                    score += 100;
                    lblPuntuacion.Text = score.ToString();
                    pbNivel.Text = nivel.ToString();
                }
            }
            #endregion

            //Evaluación de Nivel
            switch (score)
            {
                case 600:
                    nivel = 2;
                    break;
                case 1200:
                    nivel = 3;
                    break;
                case 1800:
                    nivel = 4;
                    break;
                case 2400:
                    nivel = 5;
                    break;
            }

            //Evaluación de velocidad
            switch(nivel)
            {
                case 1: velocidad = 5;
                    break;
                case 2:
                    velocidad = 7;
                    break;
                case 3:
                    velocidad = 10;
                    break;
                case 4:
                    velocidad = 15;
                    break;
                case 5:
                    velocidad = 20;
                    break;
            }
            //Se mueven las naves conforme avanza el timer, y aumenta la velovidad en base al nivel
            pbShip.Location = new Point(pbShip.Location.X,pbShip.Location.Y + velocidad);
            pbShip2.Location = new Point(pbShip2.Location.X, pbShip2.Location.Y + velocidad);
            pbShip3.Location = new Point(pbShip3.Location.X, pbShip3.Location.Y + velocidad);
            
        }

        private void frmJuego_Load(object sender, EventArgs e)
        {

        }
    }
}
