﻿namespace IS2
{
    partial class frmBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSalir = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblSeleccion = new System.Windows.Forms.Label();
            this.lblWord = new System.Windows.Forms.Label();
            this.chkWord = new System.Windows.Forms.CheckBox();
            this.chkExcel = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkPower = new System.Windows.Forms.CheckBox();
            this.lblPower = new System.Windows.Forms.Label();
            this.chkPdf = new System.Windows.Forms.CheckBox();
            this.lblPdf = new System.Windows.Forms.Label();
            this.chkJpg = new System.Windows.Forms.CheckBox();
            this.lblJpg = new System.Windows.Forms.Label();
            this.chkPng = new System.Windows.Forms.CheckBox();
            this.lblPng = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRuta = new System.Windows.Forms.TextBox();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.fbdGuardar = new System.Windows.Forms.FolderBrowserDialog();
            this.btnSeleccionarOrigen = new System.Windows.Forms.Button();
            this.txtRutaOrigen = new System.Windows.Forms.TextBox();
            this.lblOrigen = new System.Windows.Forms.Label();
            this.fbdOrigen = new System.Windows.Forms.FolderBrowserDialog();
            this.pbRespaldar = new System.Windows.Forms.PictureBox();
            this.pbRespaldo = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pbRespaldar)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSalir
            // 
            this.lblSalir.AutoSize = true;
            this.lblSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSalir.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalir.ForeColor = System.Drawing.Color.Teal;
            this.lblSalir.Location = new System.Drawing.Point(12, 373);
            this.lblSalir.Name = "lblSalir";
            this.lblSalir.Size = new System.Drawing.Size(54, 25);
            this.lblSalir.TabIndex = 7;
            this.lblSalir.Text = "Salir";
            this.lblSalir.Click += new System.EventHandler(this.lblSalir_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.Teal;
            this.lblTitulo.Location = new System.Drawing.Point(58, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(212, 25);
            this.lblTitulo.TabIndex = 12;
            this.lblTitulo.Text = "Respaldo de Archivos";
            // 
            // lblSeleccion
            // 
            this.lblSeleccion.AutoSize = true;
            this.lblSeleccion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSeleccion.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeleccion.ForeColor = System.Drawing.Color.Teal;
            this.lblSeleccion.Location = new System.Drawing.Point(12, 109);
            this.lblSeleccion.Name = "lblSeleccion";
            this.lblSeleccion.Size = new System.Drawing.Size(233, 17);
            this.lblSeleccion.TabIndex = 13;
            this.lblSeleccion.Text = "Seleccionar archivos a respaldar:";
            // 
            // lblWord
            // 
            this.lblWord.AutoSize = true;
            this.lblWord.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblWord.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWord.ForeColor = System.Drawing.Color.Teal;
            this.lblWord.Location = new System.Drawing.Point(101, 158);
            this.lblWord.Name = "lblWord";
            this.lblWord.Size = new System.Drawing.Size(155, 17);
            this.lblWord.TabIndex = 14;
            this.lblWord.Text = "Documentos de Word";
            // 
            // chkWord
            // 
            this.chkWord.AutoSize = true;
            this.chkWord.Location = new System.Drawing.Point(277, 161);
            this.chkWord.Name = "chkWord";
            this.chkWord.Size = new System.Drawing.Size(15, 14);
            this.chkWord.TabIndex = 15;
            this.chkWord.UseVisualStyleBackColor = true;
            // 
            // chkExcel
            // 
            this.chkExcel.AutoSize = true;
            this.chkExcel.Location = new System.Drawing.Point(277, 144);
            this.chkExcel.Name = "chkExcel";
            this.chkExcel.Size = new System.Drawing.Size(15, 14);
            this.chkExcel.TabIndex = 17;
            this.chkExcel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(145, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Libros de Excel";
            // 
            // chkPower
            // 
            this.chkPower.AutoSize = true;
            this.chkPower.Location = new System.Drawing.Point(277, 178);
            this.chkPower.Name = "chkPower";
            this.chkPower.Size = new System.Drawing.Size(15, 14);
            this.chkPower.TabIndex = 19;
            this.chkPower.UseVisualStyleBackColor = true;
            // 
            // lblPower
            // 
            this.lblPower.AutoSize = true;
            this.lblPower.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPower.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPower.ForeColor = System.Drawing.Color.Teal;
            this.lblPower.Location = new System.Drawing.Point(40, 175);
            this.lblPower.Name = "lblPower";
            this.lblPower.Size = new System.Drawing.Size(216, 17);
            this.lblPower.TabIndex = 18;
            this.lblPower.Text = "Presentaciones de Power Point";
            // 
            // chkPdf
            // 
            this.chkPdf.AutoSize = true;
            this.chkPdf.Location = new System.Drawing.Point(277, 195);
            this.chkPdf.Name = "chkPdf";
            this.chkPdf.Size = new System.Drawing.Size(15, 14);
            this.chkPdf.TabIndex = 21;
            this.chkPdf.UseVisualStyleBackColor = true;
            // 
            // lblPdf
            // 
            this.lblPdf.AutoSize = true;
            this.lblPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPdf.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPdf.ForeColor = System.Drawing.Color.Teal;
            this.lblPdf.Location = new System.Drawing.Point(135, 192);
            this.lblPdf.Name = "lblPdf";
            this.lblPdf.Size = new System.Drawing.Size(121, 17);
            this.lblPdf.TabIndex = 20;
            this.lblPdf.Text = "Documentos PDF";
            // 
            // chkJpg
            // 
            this.chkJpg.AutoSize = true;
            this.chkJpg.Location = new System.Drawing.Point(277, 212);
            this.chkJpg.Name = "chkJpg";
            this.chkJpg.Size = new System.Drawing.Size(15, 14);
            this.chkJpg.TabIndex = 23;
            this.chkJpg.UseVisualStyleBackColor = true;
            // 
            // lblJpg
            // 
            this.lblJpg.AutoSize = true;
            this.lblJpg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblJpg.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJpg.ForeColor = System.Drawing.Color.Teal;
            this.lblJpg.Location = new System.Drawing.Point(155, 209);
            this.lblJpg.Name = "lblJpg";
            this.lblJpg.Size = new System.Drawing.Size(101, 17);
            this.lblJpg.TabIndex = 22;
            this.lblJpg.Text = "Imagenes JPG";
            // 
            // chkPng
            // 
            this.chkPng.AutoSize = true;
            this.chkPng.Location = new System.Drawing.Point(277, 229);
            this.chkPng.Name = "chkPng";
            this.chkPng.Size = new System.Drawing.Size(15, 14);
            this.chkPng.TabIndex = 25;
            this.chkPng.UseVisualStyleBackColor = true;
            // 
            // lblPng
            // 
            this.lblPng.AutoSize = true;
            this.lblPng.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPng.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPng.ForeColor = System.Drawing.Color.Teal;
            this.lblPng.Location = new System.Drawing.Point(155, 226);
            this.lblPng.Name = "lblPng";
            this.lblPng.Size = new System.Drawing.Size(106, 17);
            this.lblPng.TabIndex = 24;
            this.lblPng.Text = "Imagenes PNG";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(12, 273);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Cambiar ruta de respaldo:";
            // 
            // txtRuta
            // 
            this.txtRuta.Location = new System.Drawing.Point(17, 294);
            this.txtRuta.Name = "txtRuta";
            this.txtRuta.Size = new System.Drawing.Size(244, 20);
            this.txtRuta.TabIndex = 27;
            this.txtRuta.Text = "C:\\respaldos";
            // 
            // btnExaminar
            // 
            this.btnExaminar.Location = new System.Drawing.Point(268, 294);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(32, 23);
            this.btnExaminar.TabIndex = 28;
            this.btnExaminar.Text = "...";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // btnSeleccionarOrigen
            // 
            this.btnSeleccionarOrigen.Location = new System.Drawing.Point(268, 77);
            this.btnSeleccionarOrigen.Name = "btnSeleccionarOrigen";
            this.btnSeleccionarOrigen.Size = new System.Drawing.Size(32, 23);
            this.btnSeleccionarOrigen.TabIndex = 32;
            this.btnSeleccionarOrigen.Text = "...";
            this.btnSeleccionarOrigen.UseVisualStyleBackColor = true;
            this.btnSeleccionarOrigen.Click += new System.EventHandler(this.btnSeleccionarOrigen_Click);
            // 
            // txtRutaOrigen
            // 
            this.txtRutaOrigen.Location = new System.Drawing.Point(17, 77);
            this.txtRutaOrigen.Name = "txtRutaOrigen";
            this.txtRutaOrigen.Size = new System.Drawing.Size(244, 20);
            this.txtRutaOrigen.TabIndex = 31;
            this.txtRutaOrigen.Text = "C:\\";
            // 
            // lblOrigen
            // 
            this.lblOrigen.AutoSize = true;
            this.lblOrigen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblOrigen.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrigen.ForeColor = System.Drawing.Color.Teal;
            this.lblOrigen.Location = new System.Drawing.Point(12, 56);
            this.lblOrigen.Name = "lblOrigen";
            this.lblOrigen.Size = new System.Drawing.Size(171, 17);
            this.lblOrigen.TabIndex = 30;
            this.lblOrigen.Text = "Cambiar ruta de origen:";
            // 
            // pbRespaldar
            // 
            this.pbRespaldar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbRespaldar.Image = global::IS2.Properties.Resources.ic_history_black_24dp_2x;
            this.pbRespaldar.Location = new System.Drawing.Point(252, 350);
            this.pbRespaldar.Name = "pbRespaldar";
            this.pbRespaldar.Size = new System.Drawing.Size(48, 48);
            this.pbRespaldar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbRespaldar.TabIndex = 29;
            this.pbRespaldar.TabStop = false;
            this.pbRespaldar.Click += new System.EventHandler(this.pbRespaldar_Click);
            // 
            // pbRespaldo
            // 
            this.pbRespaldo.Location = new System.Drawing.Point(15, 321);
            this.pbRespaldo.Name = "pbRespaldo";
            this.pbRespaldo.Size = new System.Drawing.Size(285, 23);
            this.pbRespaldo.TabIndex = 33;
            // 
            // frmBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 407);
            this.Controls.Add(this.pbRespaldo);
            this.Controls.Add(this.btnSeleccionarOrigen);
            this.Controls.Add(this.txtRutaOrigen);
            this.Controls.Add(this.lblOrigen);
            this.Controls.Add(this.pbRespaldar);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.txtRuta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkPng);
            this.Controls.Add(this.lblPng);
            this.Controls.Add(this.chkJpg);
            this.Controls.Add(this.lblJpg);
            this.Controls.Add(this.chkPdf);
            this.Controls.Add(this.lblPdf);
            this.Controls.Add(this.chkPower);
            this.Controls.Add(this.lblPower);
            this.Controls.Add(this.chkExcel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkWord);
            this.Controls.Add(this.lblWord);
            this.Controls.Add(this.lblSeleccion);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblSalir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBackup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmBackup";
            ((System.ComponentModel.ISupportInitialize)(this.pbRespaldar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSalir;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblSeleccion;
        private System.Windows.Forms.Label lblWord;
        private System.Windows.Forms.CheckBox chkWord;
        private System.Windows.Forms.CheckBox chkExcel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkPower;
        private System.Windows.Forms.Label lblPower;
        private System.Windows.Forms.CheckBox chkPdf;
        private System.Windows.Forms.Label lblPdf;
        private System.Windows.Forms.CheckBox chkJpg;
        private System.Windows.Forms.Label lblJpg;
        private System.Windows.Forms.CheckBox chkPng;
        private System.Windows.Forms.Label lblPng;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRuta;
        private System.Windows.Forms.Button btnExaminar;
        private System.Windows.Forms.PictureBox pbRespaldar;
        private System.Windows.Forms.FolderBrowserDialog fbdGuardar;
        private System.Windows.Forms.Button btnSeleccionarOrigen;
        private System.Windows.Forms.TextBox txtRutaOrigen;
        private System.Windows.Forms.Label lblOrigen;
        private System.Windows.Forms.FolderBrowserDialog fbdOrigen;
        private System.Windows.Forms.ProgressBar pbRespaldo;
    }
}