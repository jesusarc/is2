﻿using System;
using System.Windows.Forms;
using System.Net.Sockets; //Libreria usada para realizar conexiones TCP/IP
using System.Threading; //Libreria para crear hilos entre aplicaciones.
using System.Drawing; //Libreria que permite dibujar
using System.Net; //Libreria que permite la conexion web
using System.Runtime.Serialization.Formatters.Binary; //Libreria para conversion a datos binarios

namespace mouseRemoto
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Declaración de Variables
        /// </summary>
        private int puerto;
        private TcpClient cliente;   //Variable que recibe el puerto del cliente
        private TcpListener servidor;//Variable del puerto servidor
        private NetworkStream transmision; //Variable para realizar el envio de datos
        //Variables para la creación de hilo entre ambas aplicaciones.
        private Thread recibiendo;
        private Thread obtenerMouse;
        public Form1()
        {
            
           
            InitializeComponent();
        }
        //Salir de la aplicación
        private void lblSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //Método en el que se comienza a recibir la posicion del mouse
        private void comenzarRecepcion()
        {
            //Mientras el cliente no este conectado se inicia el servidor 
            while (!cliente.Connected)
            {
                servidor.Start();
                cliente = servidor.AcceptTcpClient();
            }
            //Se obtiene la imagen a mostrar.
            obtenerMouse.Start();
        }
        //Método que detiene la recepcion de datos.
        private void detenerRecepcion()
        {
            //Se detiene el servidor y el cliente se borra.
            servidor.Stop();
            cliente = null;
            //Si existe la conexion y muestra de imagen se termina con ambas.
            if (recibiendo.IsAlive) recibiendo.Abort();
            if (obtenerMouse.IsAlive) obtenerMouse.Abort();
        }
        //Método en el que se reciben los datos de imagen.
        private void recibirMouse()
        {
            //Se transforman los datos binarios recibidos a imagen para poder mostrar el escritorio.
            BinaryFormatter formatear = new BinaryFormatter();
            while (cliente.Connected)
            {
                transmision = cliente.GetStream();
                Cursor.Position = (Point) formatear.Deserialize(transmision);
            }
        }
        //Método que se ejecuta al cargar la forma
        private void Form1_Load(object sender, EventArgs e)
        {
            puerto = 2015;
            servidor = new TcpListener(IPAddress.Any, puerto);
            cliente = new TcpClient();
            recibiendo = new Thread(comenzarRecepcion);
            obtenerMouse = new Thread(recibirMouse);
            InitializeComponent();
            
            
            
        }
        //Método que se ejecuta al cerrar la ventana
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            detenerRecepcion();
        }

        private void btnPermitir_Click(object sender, EventArgs e)
        {
            //Se iguala el valor de la variable creada al valor que será recibido
            comenzarRecepcion();
        }
    }
}
